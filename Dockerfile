FROM node:11-alpine

RUN mkdir -p /server

WORKDIR /server

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001 

# CMD command - container executes by default when you launch the built image. 
# RUN npm run start --prod
CMD ["npm", "run", "start"]
