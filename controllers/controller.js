/**
 * This is our Controller w.r.t MVC architecture
 */

import Tree from '../models/tree.js';

module.exports.controller = (app) => {

    /**  
     * GET: Fetch all trees
    */
    /*
        curl http://localhost:3001/trees
    */
    app.get('/trees', (req, res) => {
        Tree.find({})
            .then((data)=>{
                console.log(data);
                res.send(data);
            })
            .catch((err)=>{
                console.log(`Error retrieving Tree. Error Stack: ${err}`);
            });
    });

    /**  
     * POST: Add Factory
     * Params: parentId + Factory properties
    */
    /*
        curl -d '{"parentId":"5d91421d92a189a44ff2554f", "name":"Hello Kitty", "maxChildren" : 3, "lowerBound" : 1990, "upperBound" : 2019}' -H "Content-Type: application/json" -X POST http://localhost:3001/addfactory
    */
    app.post('/addfactory', (req, res) => {
        let parentId = req.body.parentId;
        let factoryName = req.body.name;
        let maxChildren = req.body.maxChildren;
        let lowerBound = req.body.lowerBound;
        let upperBound = req.body.upperBound;
        let computedChildren = [];

        // Generate factory.children
        if(maxChildren) {
            console.log (`Children being created- Max#: ${maxChildren} - Upper: ${upperBound} & Lower ${lowerBound}`);
            for (let i=0; i < maxChildren; i++) {
                console.log (`Children being created- Upper: ${upperBound} & Lower ${lowerBound}`);
                var random = Math.floor(Math.random() * (+upperBound - +lowerBound) + +lowerBound); 
                computedChildren.push(random);
                console.log(computedChildren[i]);
            }
        }
        // Create Factroy
        const newFactory = {
            name: factoryName,
            maxChildren: maxChildren,
            lowerBound: lowerBound,
            upperBound: upperBound,
            children: computedChildren
        };
        
        console.log('starting to add.. ');

        // find and add sub document
        Tree.findById(parentId)
        .then((tree) => {
            // if(!factories) { 
            //     tree.factories = [];
            // }
            // Push new factory under factories
            tree.factories.push(newFactory);
            tree.save(); // saves document with subdocuments and triggers validation
        })
        .then((tree) => {
            res.status(200).send(newFactory);
        })
        .catch((err)=>{
            res.status(500).send(err);
            console.log(`Error finding Tree. Error Stack: ${err}`);
        });

    });

    /**  
     * PUT: Update Factory
     * Params: _id + Factory properties(!children)
    */
    /*
        curl -d '{"name":"Liverpool", "upperBound" : 2019}' -H "Content-Type: application/json" -X PUT http://localhost:3001/updatefactory/5d9184caf5d2f67e80c1453e
    */
    app.put('/updatefactory/:id', (req, res) => {
        let factoryName =  req.body.name;
        let maxChildren = req.body.maxChildren;
        let lowerBound =  req.body.lowerBound;
        let upperBound =  req.body.upperBound;
        let computedChildren = [];

        // Generate factory.children
        if(maxChildren) {
            console.log (`Children being recreated- Max#: ${maxChildren} - Upper: ${upperBound} & Lower ${lowerBound}`);
            for (let i=0; i < maxChildren; i++) {
                var random = Math.floor(Math.random() * (+upperBound - +lowerBound) + +lowerBound); 
                computedChildren.push(random);
                console.log(computedChildren[i]);
            }
        } 

        Tree.findOneAndUpdate(
            {'factories._id': req.params.id},
            {   
                'factories.$.name': req.body.name, 
                'factories.$.lowerBound': req.body.lowerBound, 
                'factories.$.upperBound': req.body.upperBound,
                'factories.$.children': computedChildren,
            },
            {new: true}, 
            )
            .then((tree) => {
                res.send(tree);
                // res.json({message: 'Factory successfully updated.'});
            })
            .catch((err)=>{
                console.log(`Error updating Tree. Error Stack: ${err}`);
        });

    });

    /**  
     * DELETE: Delete Factory
     * Params: parentId, factoryId
    */
    /*
        curl -X DELETE http://localhost:3001/deletefactory/5d918e42acb50a8719bb293d
    */
    // 4. Delete Factory
    app.delete('/deletefactory/:id', (req, res) => {

        Tree.findOneAndUpdate(
            {'factories._id': req.params.id},
            {$pull: { factories: {_id: req.params.id }}}, 
            {new: true}, 
            function(err, tree) {
                if(err) res.send(err);
                res.json({message: 'Factory successfully deleted.'});
            });
    });

};
