/**
 * Models are saved using mongoose schema
 * Leveraging mongoose validators
 *  */ 

import mongoose from "mongoose";

// Setup schema
// const Factory = mongoose.Schema({
// var ObjectID = mongoose.Types.ObjectId;

const Factory = {
    name: String,
    maxChildren: Number,
    lowerBound: Number,
    upperBound: Number,
    children: [Number]
};

export default Factory;
