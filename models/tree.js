/**
 * Models are saved using mongoose schema
 *  */ 

import mongoose from "mongoose";
import Factory from "./factory.js";

// Setup schema
const treeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    isRoot: {
        type: Boolean,
        required: true
    },
    // using embedded document approach
    factories: {
        type: [Factory],
        default: undefined
    }
});

// Attaching schema to actual model/document in mongo
const Tree = mongoose.model('trees', treeSchema);    

export default Tree;
