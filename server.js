import express from "express";
import cors from "cors";
import morgan from "morgan";
import mongoose from "mongoose";
import fs from "fs";
import Tree from './models/tree.js';

const PORT = process.env.API_PORT || 3001;
const DB_ROUTE = 'mongodb://mongo:27017/pdemo';
// const DB_ROUTE = 'mongodb://localhost:27017/pdemo';

// Initialize the app, router
const app = express(); 
const router = express.Router();

// Initialize middleware
app.use(cors());
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/', router);

// API routes in the app
router.get('/', (req, res) => {
    res.json({ message: 'API Initialized!'});
});

// Setup routing : importing files under /controllers
fs.readdirSync("controllers").forEach(function (file) {
    if(file.substr(-3) == ".js") {
      const route = require("./controllers/" + file);
      route.controller(app);
} })

/*
DeprecationWarning: current Server Discovery and Monitoring engine is
deprecated, and will be removed in a future version. To use the new Server
Discover and Monitoring engine, pass option { useUnifiedTopology: true } to
the MongoClient constructor
*/
mongoose.set('useUnifiedTopology', true);

// Database: must use the useNewUrlParser for mongo >=3.1.0
mongoose.connect(DB_ROUTE, { useNewUrlParser: true }, function(err) {
    if (err) {
        console.error(
            `DB Connection Error: Check if MongoDB is running.
            Error: ${err.stack}`
        );
        console.error('Shutting the server... Fix and restart');
        process.exit(1);
    }
    else {
        console.log('Connection to MongoDB has been made');
    }
});

mongoose.connection
  .on('error', function(err) {
    console.error('DB Error: Could not connect. Check if MongoDB deamon is running, Error:', err.stack);
  });

// Log all queries fired by Mongoose
mongoose.set('debug', true);

// seeding data
function mySeeder() {

    var myObjectID =  mongoose.Types.ObjectId;
    
    const seed = new Tree(
        {
            "name" : "Premier League Wins",
            "isRoot" : true,
            "factories" : [
                { 
                    "_id" : new myObjectID(), 
                    "name": "Chelsea", 
                    "maxChildren": 4, 
                    "lowerBound" : 1800, 
                    "upperBound" : 2019,
                    "children" : [ 1990, 1890, 1998, 1995 ]
                },
                { 
                    "_id" : new myObjectID(), 
                    "name": "Arsenal", 
                    "maxChildren": 3, 
                    "lowerBound" : 1990, 
                    "upperBound" : 2019,
                    "children" : [ 1990, 1991, 2001 ]
                },
                { 
                    "_id" : new myObjectID(), 
                    "name": "Brighton", 
                    "maxChildren": 1, 
                    "lowerBound" : 1890, 
                    "upperBound" : 2010,
                    "children" : [ 2000 ]
                }
    
            ]
        }
    );
    console.log("Seeding done");
    seed.save();
}

mySeeder();

app.listen(PORT, () =>
    console.log(`API server running on port ${PORT}`)
);

module.exports.controller = (app) => {
    console.log("This is server module");
}
